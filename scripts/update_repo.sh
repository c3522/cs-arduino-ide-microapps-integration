#!/bin/bash
# Script to automaticaly publish files needed by Arduino IDE to another repo which serves as server to download "compiled" files from.

PWD=$pwd
REPO_PWD=/home/m/.arduino-generated-files/

tar -cvjSf $REPO_PWD/cs-arduino-platform.tar.bz2 platform/
cp platform/package_Crownstone_Microapps_index.json $REPO_PWD

cd $REPO_PWD
git add -A
git commit -m "Auto generated commit"
git push --set-upstream origin master
cd $PWD
